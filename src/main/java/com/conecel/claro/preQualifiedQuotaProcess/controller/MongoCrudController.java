package com.conecel.claro.preQualifiedQuotaProcess.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.conecel.claro.preQualifiedQuotaProcess.entity.Parametros;
import com.conecel.claro.preQualifiedQuotaProcess.service.CrudMongo;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/mongocluster")
public class MongoCrudController {

	@Autowired
	CrudMongo crud;
	
	@GetMapping({"","/{id}"})
	public Object mongofind(@PathVariable(required = false, name="id") String id) {
		if(id == null) {
			return crud.testselectall();
		}else {
			return crud.testselect(id);
		}
		
	}


	@PostMapping
	public Parametros mongosave(@RequestBody @Valid Parametros pre) {
		return crud.testsave(pre);
	}

}
