package com.conecel.claro.preQualifiedQuotaProcess;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;



@SpringBootApplication
@ComponentScan(basePackages = { "com.conecel.claro.preQualifiedQuotaProcess","com.conecel.claro.preQualifiedQuotaProcess.config","com.conecel.claro.preQualifiedQuotaProcess.controller","com.conecel.claro.preQualifiedQuotaProcess.entity","com.conecel.claro.preQualifiedQuotaProcess.service" })
public class MongoClusterTestingApplication {

	
	public static void main(String[] args) {
		SpringApplication.run(MongoClusterTestingApplication.class, args);		
	}

}
