package com.conecel.claro.preQualifiedQuotaProcess.service;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.conecel.claro.preQualifiedQuotaProcess.entity.Parametros;
import com.conecel.claro.preQualifiedQuotaProcess.entity.IParametros;



@Service 
public class CrudMongo {
	
	private static final Log log = LogFactory.getLog(CrudMongo.class);

	@Autowired
	private IParametros iparametro;
	
	public List<Parametros> testselectall(){
		
		List<Parametros> parametros;
		boolean insert=true;
		int n=1;
		do {
		try {
			log.info("Intento N@"+n);
			parametros = iparametro.findAll();
			log.info(">>> correcto <<<"+"Intento N@"+n);
			insert=false;
			}catch(Exception ex){
				log.error(">>> fail <<<"+"Intento N@"+n);
				parametros = iparametro.findAll();
				log.error(ex.getMessage());
				n++;
				if (n==4){log.fatal("Numero de reintentos maximo alcansado"+"Intento N@"+n);}
			}
		}while(insert);
		return parametros;
	}

	public Parametros testselect(String id_PrequalifiedQuoteV1){
		
		Parametros parametros;
		boolean insert=true;
		int n=1;
		do {
		try {
			log.info("Intento N@"+n);
			parametros = iparametro.findBy_id(id_PrequalifiedQuoteV1);
			log.info(">>> correcto <<<"+"Intento N@"+n);
			insert=false;
			}catch(Exception ex){
				log.error(">>> fail <<<"+"Intento N@"+n);
				parametros = iparametro.findBy_id(id_PrequalifiedQuoteV1);
				log.error(ex.getMessage());
				n++;
				if (n==4){log.fatal("Numero de reintentos maximo alcansado"+"Intento N@"+n);}
			}
		}while(insert);
		return parametros;
	}
	
	public Parametros testsave(Parametros parametro)
	{ 
		boolean insert=true;
		int n=1;
		do {
		try {
			log.info(parametro.toString());
			iparametro.save(parametro);
			log.info(">>> correcto <<<"+"Intento N@"+n);
			insert=false;
			}catch(Exception ex){
				log.error(">>> fail <<<"+"Intento N@"+n);
				log.error(parametro.toString());
				log.error(ex.getMessage());
				n++;
				if (n==4)
				{
					log.fatal("Numero de reintentos maximo alcansado"+"Intento N@"+n); 
					parametro.set_id(null);
					insert=false;
				}
			}
		}while(insert);
		return parametro;
	}
	
}
