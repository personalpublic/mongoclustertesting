package com.conecel.claro.preQualifiedQuotaProcess.entity;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Parametrostest")
public class Parametros implements Serializable{
	@Override
	public String toString() {
		return "Parametros [_id=" + _id + ", description=" + description + ", state=" + state + ", value=" + value
				+ "]";
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
    private String _id;
    private String description;
    private String state;
    private String value;
	public String get_id() {
		return _id;
	}
	public void set_id(String _id) {
		this._id = _id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
}
