package com.conecel.claro.preQualifiedQuotaProcess.entity;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.conecel.claro.preQualifiedQuotaProcess.entity.Parametros;

public interface IParametros extends MongoRepository<Parametros, String>{

	public Parametros findBy_id(String _id);
	
	public List<Parametros> findAll();
	
}